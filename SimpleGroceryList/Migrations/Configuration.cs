namespace SimpleGroceryList.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SimpleGroceryList.Models.SimpleGroceryListContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SimpleGroceryList.Models.SimpleGroceryListContext context)
        {

        }
    }
}
